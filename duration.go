package core

import (
	"time"
	"strconv"
)

//Duration a elapsed duration of time.
type Duration time.Duration

//UnmarshalJSON used to unmarshal a duration string into a Duration.
func (d *Duration) UnmarshalJSON(data []byte) error {
	s := string(data)
	if s == "null" {
		return nil
	}

	s, err := strconv.Unquote(s)
	if err != nil {
		return err
	}

	t, err := time.ParseDuration(s)
	if err != nil {
		return err
	}

	*d = Duration(t)

	return nil
}
