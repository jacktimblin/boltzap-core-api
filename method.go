package core

import (
	"net/http"
	"strings"
	"encoding/json"
)

//Method an encapsulation of a http method.
type Method string

//Method allows use to unmarshal Method from json.
func (m *Method) UnmarshalJSON(data []byte) error {
	method := new(string)
	if err := json.Unmarshal(data, method); err != nil {
		return err
	}

	*m = Method(strings.ToUpper(*method))

	return nil
}

//IsValid determines if the method is valid.
func (m *Method) IsValid() bool {
	switch string(*m) {
	case http.MethodGet:
	case http.MethodDelete:
	case http.MethodPost:
	case http.MethodPut:
	case http.MethodTrace:
	case http.MethodPatch:
		return true
	}

	return false
}

//String implement String interface.
func (m *Method) String() string {
	return string(*m)
}
