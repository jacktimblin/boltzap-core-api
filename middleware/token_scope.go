package service

import (
	"core/service"

	"github.com/labstack/echo"
)

//JWTTokenScope middleware which checks a token has the required scope.
func JWTTokenScope(scope int, auth *service.AuthService) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			user, tkn, err := auth.GetAuthDataFromContext(c)

			if err != nil {
				return echo.ErrUnauthorized
			}

			token, _, found := user.GetToken(tkn, true)
			if !found {
				return echo.ErrUnauthorized
			}

			if !token.HasScope(scope) {
				return echo.ErrUnauthorized
			}

			return next(c)
		}
	}
}
