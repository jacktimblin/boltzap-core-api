package service

import (
	"core/model"
	"core/service"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

//JWTWithDatabaseCheck Used to confirm a user is authorised by using the JWT token.
func JWTWithDatabaseCheck(auth *service.AuthService) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			//get the token.
			token, ok := c.Get(auth.GetContextKey()).(*jwt.Token)
			if !ok {
				return echo.ErrUnauthorized
			}
			//perform a check on the token.
			claims, ok := token.Claims.(*model.UserClaims)

			if !ok {
				return echo.ErrUnauthorized
			}

			user, found, err := auth.FindUserByJWTToken(token)

			if err != nil {
				return echo.ErrUnauthorized
			}

			if !found {
				return echo.ErrUnauthorized
			}

			if claims.UUID == user.Id.Hex() {
				//set the user in the current context.
				auth.SetAuthDataFromContext(c, user)
				return next(c)
			}

			return echo.ErrUnauthorized
		}
	}
}
