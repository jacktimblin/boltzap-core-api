package event

import (
	"encoding/json"
	"errors"

	"gopkg.in/mgo.v2/bson"
)

//QueuePositionTopic The queue position topic used on NSQ.
const QueuePositionTopic = "queue_position"

//Queue An event to process a users position in the queue.
type Queue struct {
	UserID bson.ObjectId `json:"user_id"`
	//the amount of positions to push the user up the queue.
	Amount int `json:"amount"`
}

//UnmarshalJSON Unmarshals an queue event from JSON. Required as UserId is a bson.ObjectId
func (q *Queue) UnmarshalJSON(buf []byte) error {
	var raw map[string]interface{}

	err := json.Unmarshal(buf, &raw)

	if err != nil {
		return err
	}

	id, ok := raw["user_id"].(string)

	if !ok || !bson.IsObjectIdHex(id) {
		return errors.New("user id must be set and must be a hexidecimal string")
	}

	q.UserID = bson.ObjectIdHex(id)

	amount, ok := raw["amount"].(float64)

	if !ok {
		return errors.New("amount must be numeric")
	}

	q.Amount = int(amount)

	return nil
}
