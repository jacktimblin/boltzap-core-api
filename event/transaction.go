package event

//TransactionTopic The topic for transaction events.
const TransactionTopic = "transactions"

const (
	//TransactionCreated The event type for transaction created events.
	TransactionCreated = 1
)

//TransactionCreated The event passed from any transaction processor to NSQ so we can process this event
//in accounting software or any other source which is attached to the relevant topic.
type Transaction struct {
	//UUID the unique user id.
	UUID string `json:"uuid"`
	//EventID The unique gateway ID for the transactions created.
	EventID string `json:"event_id"`
}
