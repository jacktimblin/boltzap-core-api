package event

import "core"

//TODO - Endpoint should be core.URL.

//Webhook an encapsulation of a webhook event.
type Webhook struct {
	//Event the event that occurred.
	Event      interface{} `json:"event"`
	//RetryCount the amount of attempted retries of this event.
	RetryCount int         `json:"retry_count"`
	//Endpoint The endpoint to send the event to.
	Endpoint   core.URL   `json:"endpoint"`
	//Gateway the gateway the event is for.
	Gateway    int         `json:"gateway"`
	//EventID The request event id.
	EventID    string      `json:"event_id"`
	//Type the type of event that occurred.
	Type 	   int		   `json:"type"`
	//Method the http method to use.
	Method     core.Method `json:"method"`
}
