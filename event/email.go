package event

import (
	"encoding/json"
	"errors"
)

const (
	//EmailTopic The topic where email requests are sent on NSQ.
	EmailTopic = "email_queue"
	//TypeVerifyEmail The verify email type.
	TypeVerifyEmail = 0
	//TypePasswordReset The password reset email type.
	TypePasswordReset = 2
	//TypePasswordResetConfirm The password reset confirmed email type.
	TypePasswordResetConfirm = 3
	//TypeDeauthorisedAccount The email type when a user de-authorises a third party with us.
	TypeDeauthorisedAccount = 4
	//TypeSubscriptionPaymentFailed The email type when a users subscription payment fails.
	TypeSubscriptionPaymentFailed = 5
	//TypeSubscriptionPaymentSucceeded The email type when a users subscription succeeds.
	TypeSubscriptionPaymentSucceeded = 6
	//TypeSubscriptionDeleted The email type when a users subscription is cancelled.
	TypeSubscriptionDeleted = 7
	//TypeSubscriptionFreeTrialEnding The email type when a users subscriptions free trial is coming to an end.
	TypeSubscriptionFreeTrialEnding = 8
	//TypeSubscriptionCreated the email type when a user has just signed up for a subscription.
	TypeSubscriptionCreated = 9
	TypePoppedQueue
)

//ProviderType the type of provider.
type ProviderType string

const  (
	ProviderTypePaymentGateway ProviderType = "payment_gateway"
)

//Metadata Any metadata to pass with an email event.
type Metadata map[string]interface{}

//Email An email event passed to NSQ.
type Email struct {
	UUID     		string   `json:"user_id"`
	Type     		int      `json:"type"`
	Metadata 		Metadata `json:"metadata"`
	TransactionUUID string   `json:"transaction_id"`
}

//UnmarshalJSON Unmarshals an email event from JSON. Required as UserId is a bson.ObjectId
func (e *Email) UnmarshalJSON(buf []byte) error {
	var raw map[string]interface{}

	err := json.Unmarshal(buf, &raw)

	if err != nil {
		return err
	}

	id, ok := raw["user_id"].(string)

	if !ok {
		return errors.New("user id must be set and be a string")
	}

	e.UUID = id

	typ, ok := raw["type"].(float64)

	if !ok {
		return errors.New("type must be numeric")
	}

	e.Type = int(typ)

	metadata, ok := raw["metadata"].(map[string]interface{})

	if ok {
		e.Metadata = metadata
	}

	if transactionUUID, ok := raw["transaction_id"].(string); ok {
		e.TransactionUUID = transactionUUID
	}

	return nil
}
