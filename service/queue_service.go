package service

import (
	"encoding/json"

	"github.com/google/uuid"
	"github.com/nsqio/go-nsq"
)

const (
	//DefaultNSQDAddress the default nsqd address.
	DefaultNSQDAddress = "127.0.0.1:4150"
	//ConfigQueue the key in config to find queue service config.
	ConfigQueue = "queue"
)

//QueueMessage A message to send to the queue.
type QueueMessage struct {
	Key      uuid.UUID
	Topic    string
	Message  interface{}
	Callback QueueCallback
}

//QueueCallback A callback when we receive a response from NSQ.
type QueueCallback func(message QueueMessage, err error)

//QueueService The service which publishes QueueMessage's to an NSQ instance.
type QueueService struct {
	NsqAddress string
	producer   *nsq.Producer
	Ready      bool
}

//QueueConfig the config for the queue.
type QueueConfig struct {
	NsqAddress string `json:"nsq_address"`
}

//NewQueueConfig generates new queue config.
func NewQueueConfig() QueueConfig {
	return QueueConfig{
		NsqAddress: DefaultNSQDAddress,
	}
}

//NewQueueService Initialises a new Queue Service.
func NewQueueService(config QueueConfig) *QueueService {
	qs := &QueueService{
		NsqAddress: config.NsqAddress,
		Ready:      true,
	}

	pc := nsq.NewConfig()
	p, err := nsq.NewProducer(qs.NsqAddress, pc)

	if err != nil {
		panic(err)
	}

	qs.producer = p

	return qs
}

//PublishBulk Sends multiple messages asyncronously.
func (q *QueueService) PublishBulk(messages []QueueMessage) {
	for _, message := range messages {
		go func(message QueueMessage) {
			q.PublishMessage(message)
		}(message)
	}
}

//PublishMessage Publishes a message to NSQ, without waiting for a response, receiving the response via to message.Callback.
func (q *QueueService) PublishMessage(message QueueMessage) error {

	//give this message a unique id.
	message.Key = uuid.New()

	if message.Callback == nil {
		//set default callback.
		message.Callback = func(message QueueMessage, err error) {}
	}

	//marshall the message as JSON.
	json, err := json.Marshal(message.Message)

	if err != nil {
		return err
	}

	//make a new channel which can only receive one response, to ensure responses from other
	//goroutines don't end up on the same channel for the callback.
	responseChan := make(chan *nsq.ProducerTransaction, 1)

	//topic and json are passed twice so they are passed to the response chan as args.
	err = q.producer.PublishAsync(message.Topic, json, responseChan, message)

	//wait for the callback in another gorountine, we don't have to wait for this to finish as
	//echo is already blocking - `waiting` for requests.
	go func(responseChan chan *nsq.ProducerTransaction) {
		result := <-responseChan
		if len(result.Args) > 0 {
			message := result.Args[0].(QueueMessage)
			message.Callback(message, result.Error)
		}
	}(responseChan)

	if err != nil {
		return err
	}

	return nil
}
