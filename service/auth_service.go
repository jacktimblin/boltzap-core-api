package service

import (
	"core/event"
	"core/model"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"math/rand"
	"strconv"
	"sync"
	"time"
	"core"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/maxwellhealth/bongo"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

)

const (
	//SigningKey The key to sign tokens with. TODO - Make more secure.
	SigningKey = "69458m6u5thvtmjgvkfknbti5ut5895964590mvgjtmjtkrhtjvkfertu5bty785v,t5"
	//System The system ClientId
	System = "system"
	//ContextKey The key for the jwt token in the context.
	ContextKey = "user"
	//UserContextKey The key for the user model in the context.
	UserContextKey = "user.model"
	//PasswordHistoryPeriod The amount of months to hold a password in history.
	PasswordHistoryPeriod = 6
	//DefaultConnection The default mongo db connection string.
	DefaultConnection = "localhost"
	//DefaultAuthDatabase The default database to use when checking credentials on mongo db.
	DefaultAuthDatabase = "admin"
	//ConfigConnection the config key for database credentials.
	ConfigConnection = "connection"
	//DefaultTimeout the default db connection timeout.
	DefaultTimeout = 10 * time.Second
)

//NewAuthService Create a new Auth Service.
func NewAuthService(config *AuthConfig) (*AuthService, error) {
	//generate the signing key.
	signingKey := []byte(config.SigningKey)

	//generate jwt config.
	jwtConfig := middleware.JWTConfig{
		Claims:     &model.UserClaims{},
		SigningKey: signingKey,
		ContextKey: ContextKey,
	}

	db := config.DatabaseConnection
	var mongoConfig bongo.Config
	info := &mgo.DialInfo{
		Addrs:    db.Connection,
		Timeout:  time.Duration(db.Timeout),
		Database: db.AuthDatabase,
		Username: db.Username,
		Password: db.Password,
	}
	mongoConfig = bongo.Config{
		DialInfo: info,
	}

	mongoConfig.Database = db.Database

	//Generate the mongodb config and connect to the Database.
	connection, err := bongo.Connect(&mongoConfig)

	if err != nil {
		return &AuthService{}, err
	}

	modelService := &ModelService{
		Database:   config.DatabaseConnection,
		Connection: connection,
		Config:     &mongoConfig,
	}

	//Run indices on models.
	modelService.Indices(&model.User{}, &model.Transaction{})

	var queue *QueueService
	if config.Queue.NsqAddress != "" {
		queue = NewQueueService(config.Queue)
	}

	auth := &AuthService{
		jwtConfig:  &jwtConfig,
		connection: connection,
		signingKey: signingKey,
		Queue:      queue,
		Config:     config,
	}

	return auth, nil
}

func NewDatabaseConnection() DatabaseConnection {
	db := DatabaseConnection{
		Connection:   []string{DefaultConnection},
		Username:     "",
		Password:     "",
		AuthDatabase: DefaultAuthDatabase,
		Timeout: core.Duration(DefaultTimeout),
	}

	return db
}

//NewAuthConfig Generates new config for the auth service.
func NewAuthConfig() *AuthConfig {

	db := NewDatabaseConnection()

	config := &AuthConfig{
		SigningKey:         SigningKey,
		DatabaseConnection: db,
	}

	return config
}

//DatabaseConnection A connection to the mongo db.
type DatabaseConnection struct {
	Database     string `json:"database"`
	Connection   []string `json:"nodes"`
	Username     string `json:"username"`
	Password     string `json:"password"`
	AuthDatabase string `json:"auth_database"`
	Timeout      core.Duration `json:"timeout"`
}

//AuthConfig Configuration for the auth service.
type AuthConfig struct {
	DatabaseConnection DatabaseConnection
	SigningKey         string
	Queue              QueueConfig
}

//AuthService The service which handles the authentication of users.
type AuthService struct {
	sync.Mutex
	Config     *AuthConfig
	Queue      *QueueService
	jwtConfig  *middleware.JWTConfig
	connection *bongo.Connection
	signingKey []byte
}

//SendEmail Sends an email.
func (a *AuthService) SendEmail(email event.Email) error {
	a.Lock()
	defer a.Unlock()
	if a.Queue.Ready {
		return a.Queue.PublishMessage(QueueMessage{
			Topic: event.EmailTopic,
			Message: email,
		})
	}

	return nil
}

//ConnectStripe Adds stripe credentials to a user.
func (a *AuthService) ConnectStripe(user *model.User, stripe model.Stripe) error {
	a.Lock()
	defer a.Unlock()
	user.ConnectStripe(stripe)

	return a.SaveUser(user)
}

//RemoveStripe Removes stripe credentials from a user.
func (a *AuthService) RemoveStripe(user *model.User) error {
	a.Lock()
	defer a.Unlock()
	user.Stripe = model.Stripe{}

	return a.SaveUser(user)
}

//FindUserByStripeID Finds a user by their connected stripe ID.
func (a *AuthService) FindUserByStripeID(stripeID string) (*model.User, bool, error) {
	user := &model.User{}

	query := bson.M{"stripe.accountid": stripeID}

	err := a.connection.Collection(model.UserCollection).FindOne(query, user)

	if err != nil {
		u := &model.User{}
		_, ok := err.(*bongo.DocumentNotFoundError)
		if !ok {
			return u, false, err
		}
		return u, false, nil
	}

	return user, true, nil
}

//FindUserByStripeCustomerID Finds a user by their connected stripe customer id.
func (a *AuthService) FindUserByStripeCustomerID(stripeCustomerID string) (*model.User, bool, error) {
	user := &model.User{}

	query := bson.M{"stripecustomer.customertoken": stripeCustomerID}

	err := a.connection.Collection(model.UserCollection).FindOne(query, user)

	if err != nil {
		u := &model.User{}
		_, ok := err.(*bongo.DocumentNotFoundError)
		if !ok {
			return u, false, err
		}
		return u, false, nil
	}

	return user, true, nil
}

//GetConnection Gets the internal db connection session.
func (a *AuthService) GetConnection() *bongo.Connection {
	return a.connection
}

//RemoveToken Removes a token from a user.
func (a *AuthService) RemoveToken(user *model.User, index int) {
	user.RemoveToken(index)
	a.connection.Collection(model.UserCollection).Save(user)
}

//GetNextQueuePosition Gets the next queue position number from the db.
func (a *AuthService) GetNextQueuePosition() (int, error) {
	a.Lock()
	defer a.Unlock()

	var position int
	mp := make(map[string]interface{})

	err := a.connection.Session.DB(a.connection.Config.Database).
		C(model.UserCollection).Find(bson.M{}).
		Sort("-queueposition").Select(bson.M{"queueposition": 1, "_id": 0}).
		One(mp)

	if err != nil {
		return -1, err
	}

	position, ok := mp["queueposition"].(int)

	if !ok {
		return -1, errors.New("queue position needs to be a number")
	}

	//there are no users with a queue position set.
	if position == -1 {
		position = 0
	}

	return position + 1, nil
}

//PopQueuePosition Pops a user up the queue `amount` positions.
func (a *AuthService) PopQueuePosition(user *model.User, amount int) error {
	//lock this function to ensure we don't have concurrent queue pops which
	//change position.
	a.Lock()
	defer a.Unlock()

	var current, position int

	if user.QueuePosition == -1 {
		return nil
	}

	if user.QueuePosition == 1 {
		//user is at the top of the queue.
		return nil
	}

	current = user.QueuePosition
	position = user.QueuePosition - amount
	if position < 1 {
		position = 1
	}

	//increment the users between the new position and original position by 1.
	session := a.connection.Session
	c := session.DB(a.connection.Config.Database).C(model.UserCollection)
	c.UpdateAll(
		bson.M{"queueposition": bson.M{"$elemMatch": bson.M{"$gte": position, "$lt": current}}},
		bson.M{"$inc": bson.M{"queueposition": 1}},
	)
	//Update the users position with the updated position.
	c.Update(bson.M{"_id": user.Id}, bson.M{"queueposition": position})

	//send a message to NSQ to email the user about the position change.
	if !a.Queue.Ready {
		return nil
	}

	msg := QueueMessage{
		Topic: event.EmailTopic,
		Message: event.Email{
			UUID: user.UUID,
			Type:   event.TypePoppedQueue,
			Metadata: map[string]interface{}{
				"position": position,
			},
		},
	}

	err := a.Queue.PublishMessage(msg)

	if err != nil {
		return err
	}

	return nil
}

//SaveUser Saves a user.
func (a *AuthService) SaveUser(user *model.User) error {
	return a.connection.Collection(model.UserCollection).Save(user)
}

//FindUserByUsername Finds a user by their username.
func (a *AuthService) FindUserByUsername(username string) (*model.User, bool, error) {
	user := &model.User{}
	err := a.connection.Collection(model.UserCollection).FindOne(bson.M{"username": username}, user)
	if err != nil {
		u := &model.User{}
		_, ok := err.(*bongo.DocumentNotFoundError)
		if !ok {
			return u, false, err
		}
		return u, false, nil
	}

	return user, true, nil
}

//GetContextKey Gets the context key for the jwt token.
func (a *AuthService) GetContextKey() string {
	return a.jwtConfig.ContextKey
}

//GetAuthDataFromContext Gets and returns the user and token from c.
func (a *AuthService) GetAuthDataFromContext(c echo.Context) (*model.User, string, error) {
	u, ok := c.Get(UserContextKey).(*model.User)
	if !ok {
		return &model.User{}, "", errors.New("Could not find user from context")
	}

	tkn, ok := c.Get(a.GetContextKey()).(*jwt.Token)
	if !ok {
		return &model.User{}, "", errors.New("Could not find token from context")
	}

	return u, tkn.Raw, nil
}

//SetAuthDataFromContext Sets the user in the provided context.
func (a *AuthService) SetAuthDataFromContext(c echo.Context, u *model.User) error {
	_, ok := c.Get(a.GetContextKey()).(*jwt.Token)
	if !ok {
		return errors.New("Could not find token from context")
	}

	c.Set(UserContextKey, u)
	return nil
}

//RefreshUser refreshes a user with the DB.
func (a *AuthService) RefreshUser(u *model.User) error {
	return a.connection.Collection(model.UserCollection).FindById(u.Id, u)
}

//FindUserByContext Finds a user from the provided context.
func (a *AuthService) FindUserByContext(c echo.Context) (*model.User, bool, error) {
	tkn, ok := c.Get(a.GetContextKey()).(*jwt.Token)
	if !ok {
		return &model.User{}, false, nil
	}

	return a.FindUserByJWTToken(tkn)
}

//FindUserByJWTToken Finds a user from a jwt token.
func (a *AuthService) FindUserByJWTToken(token *jwt.Token) (*model.User, bool, error) {
	//build the signed token string.
	user := &model.User{}

	//build query for a user with this active token.
	query := bson.M{"tokens": bson.M{"$elemMatch": bson.M{"token": token.Raw, "active": true}}}

	//search for the user form the token.
	err := a.connection.Collection(model.UserCollection).FindOne(
		query,
		user,
	)

	if err != nil {
		_, ok := err.(*bongo.DocumentNotFoundError)
		if !ok {
			return &model.User{}, false, err
		}
		return &model.User{}, false, nil
	}

	return user, true, nil
}

//AddNewUser Adds a new user to the system and passes messages to NSQ.
func (a *AuthService) AddNewUser(firstname, lastname, username string, password []byte, role int) error {
	u := &model.User{}
	u.Profile.FirstName = firstname
	u.Profile.LastName = lastname
	u.Username = username
	u.Role = role
	u.EmailVerified = false
	//set a non positive position in the queue.
	u.QueuePosition = -1

	u.EmailToken = model.Token{
		ClientID:  System,
		ExpiresAt: time.Now().Add(1 * time.Hour).Unix(),
		Token:     a.generateToken(*u),
		Scope:     []int{model.ScopeAll},
		Active:    true,
	}

	u.SetPassword(password)

	//save the new user.
	err := a.SaveUser(u)

	if err != nil {
		return err
	}

	if !a.Queue.Ready {
		return nil
	}

	//generate messages to send to the queue.
	messages := []QueueMessage{
		QueueMessage{
			Topic: event.QueuePositionTopic,
			Message: event.Queue{
				UserID: u.Id,
			},
		},
		QueueMessage{
			Topic: event.EmailTopic,
			Message: event.Email{
				Type:   event.TypeVerifyEmail,
				UUID: u.UUID,
			},
		},
	}

	a.Queue.PublishBulk(messages)

	return nil
}

//SendVerifyEmailRequest Sends a new verify email request email and sets a new token.
func (a *AuthService) SendVerifyEmailRequest(user *model.User) error {
	user.EmailToken = model.Token{
		ClientID:  System,
		ExpiresAt: time.Now().Add(1 * time.Hour).Unix(),
		Token:     a.generateToken(*user),
		Scope:     []int{model.ScopeAll},
		Active:    true,
	}

	go func(user *model.User, a *AuthService) {
		a.SaveUser(user)
	}(user, a)

	if !a.Queue.Ready {
		return nil
	}

	return a.Queue.PublishMessage(QueueMessage{
		Topic: event.EmailTopic,
		Message: event.Email{
			UUID: user.UUID,
			Type:   event.TypeVerifyEmail,
		},
	})
}

//FindUserByID Finds a user by its id.
func (a *AuthService) FindUserByID(id bson.ObjectId) (*model.User, bool, error) {
	user := &model.User{}
	err := a.connection.Collection(model.UserCollection).FindById(id, user)

	if err != nil {
		_, ok := err.(*bongo.DocumentNotFoundError)
		if !ok {
			return &model.User{}, false, err
		}
		return &model.User{}, false, nil
	}

	return user, true, nil
}

//FindUserByUUID Finds a user by a uuid.
func (a *AuthService) FindUserByUUID(uuid string) (*model.User, bool, error) {
	user := &model.User{}
	query := bson.M{"uuid" : uuid}

	err := a.connection.Collection(model.UserCollection).FindOne(query, user)

	if err != nil {
		_, ok := err.(*bongo.DocumentNotFoundError)
		if !ok {
			return &model.User{}, false, err
		}
		return &model.User{}, false, nil
	}

	return user, true, nil
}

//GeneratePasswordResetRequest Generates a password reset token, assigns it to
//the user and returns the generated token.
func (a *AuthService) GeneratePasswordResetRequest(u *model.User) (model.Token, error) {
	if u.PasswordResetToken.Active {
		tme := time.Unix(u.PasswordResetToken.ExpiresAt, 0)
		if tme.Before(time.Now()) {
			//user already as a valid password reset token.
			return u.PasswordResetToken, nil
		}
	}

	token := model.Token{
		ClientID:  System,
		ExpiresAt: time.Now().Add(5 * time.Minute).Unix(),
		Token:     a.generateToken(*u),
		Scope:     []int{model.ScopeAll},
		Active:    true,
	}

	u.PasswordResetToken = token
	go func(u *model.User, a *AuthService) {
		a.SaveUser(u)
	}(u, a)

	return token, nil
}

//SetPassword Sets a password for a user and notifies the queue to send the confirmation email.
func (a *AuthService) SetPassword(u *model.User, password []byte) error {

	if u.IsPasswordInHistory(password, PasswordHistoryPeriod) {
		return errors.New("Password is already in history")
	}

	go func(u *model.User, a *AuthService) {
		u.SetPassword(password)
		a.SaveUser(u)
	}(u, a)

	if !a.Queue.Ready {
		return nil
	}

	err := a.Queue.PublishMessage(QueueMessage{
		Topic: event.EmailTopic,
		Message: event.Email{
			Type:   event.TypePasswordResetConfirm,
			UUID: u.UUID,
		},
	})

	if err != nil {
		return err
	}

	return nil
}

//generateToken Generates a random token string.
func (a *AuthService) generateToken(u model.User) string {
	hash := sha256.New()
	hash.Write([]byte(strconv.Itoa(rand.Int())))
	hash.Write([]byte(u.Username))
	sha := hash.Sum([]byte(u.GetFullName()))

	return base64.URLEncoding.EncodeToString(sha)
}

//RemoveExpiredTokens Removes any expired tokens from a user.
func (a *AuthService) RemoveExpiredTokens(user *model.User) {
	if user.RemoveExpiredTokens() {
		a.SaveUser(user)
	}
}

//CreateNewToken Creates a new login token for a user.
func (a *AuthService) CreateNewToken(u *model.User) (string, error) {
	//generate the claim from the user and also store the generated token in the DB.
	//this defeats the object of stateless JWT, but I need a stateful way
	//to manage authenticated state.
	claims := &model.UserClaims{
		Name: u.GetFullName(),
		Role: u.Role,
		UUID: u.Id.Hex(),
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 1).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	signature, err := token.SignedString(a.signingKey)
	if err != nil {
		return "", err
	}

	go func() {
		//a client is not needed for this type of authentication, its a system log in.
		t := model.Token{
			Token:     signature,
			ClientID:  System,
			ExpiresAt: claims.ExpiresAt,
			Active:    true,
			Scope:     []int{model.ScopeAll},
		}

		//save the token to the user and persist the document.
		u.AddToken(t)
		a.SaveUser(u)
	}()

	return signature, nil
}

//GetJWTConfig Gets the JWT config.
func (a *AuthService) GetJWTConfig() *middleware.JWTConfig {
	return a.jwtConfig
}

//FindUserByEmailToken Finds a user by a valid verify email token.
func (a *AuthService) FindUserByEmailToken(token string) (*model.User, bool, error) {
	user := &model.User{}

	query := bson.M{"emailtoken.token": token, "emailtoken.expiresat": bson.M{"$gte": time.Now().Unix()}}

	err := a.connection.Collection(model.UserCollection).FindOne(query, user)

	if err != nil {
		_, ok := err.(*bongo.DocumentNotFoundError)
		if !ok {
			return &model.User{}, false, err
		}
		return &model.User{}, false, nil
	}

	return user, true, nil
}

//FindUserByPasswordToken Finds a user by a reset password token.
func (a *AuthService) FindUserByPasswordToken(token string) (*model.User, bool, error) {
	user := &model.User{}

	query := bson.M{"passwordtoken.token": token, "passwordtoken.expiresat": bson.M{"$gte": time.Now().Unix()}}

	err := a.connection.Collection(model.UserCollection).FindOne(query, user)

	if err != nil {
		_, ok := err.(*bongo.DocumentNotFoundError)
		if !ok {
			return &model.User{}, false, err
		}
		return &model.User{}, false, nil
	}

	return user, true, nil
}

//MarkUserEmailConfirmed Marks a users email as confirmed.
//TODO - do we need to send an email for this?
func (a *AuthService) MarkUserEmailConfirmed(user *model.User) {
	user.EmailVerified = true
	a.SaveUser(user)
}
