package service

import (
	"core/model"
	"github.com/maxwellhealth/bongo"
	"github.com/stripe/stripe-go"
	"gopkg.in/mgo.v2/bson"
)

//NewTransactionService generates a new transaction service.
func NewTransactionService(connection *bongo.Connection, queue *QueueService) *TransactionService {
	return &TransactionService{
		connection: connection,
		queue: queue,
	}
}

//TODO - handle multiple services.
//TransactionService a service used to interact with transactions.
type TransactionService struct {
	connection *bongo.Connection
	queue *QueueService
}

//FindTransaction finds a transaction based on the gateway and gateway id.
func (t *TransactionService) FindTransaction(gateway int, gatewayID string) (*model.Transaction, bool, error) {
	query := bson.M{"gateway" : gateway, "gatewayid" : gatewayID}
	transaction := &model.Transaction{}
	err := t.connection.Collection(model.TransactionCollection).FindOne(query, transaction)
	if err != nil {
		_, ok := err.(*bongo.DocumentNotFoundError)
		if !ok {
			return &model.Transaction{}, false, err
		}
		return &model.Transaction{}, false, nil
	}

	return transaction, true, nil
}

//FindTransactionByUUID finds a transaction based on the transaction UUID.
func (t *TransactionService) FindTransactionByUUID(_uuid string) (*model.Transaction, bool, error) {
	query := bson.M{"uuid" : _uuid}
	transaction := &model.Transaction{}
	err := t.connection.Collection(model.TransactionCollection).FindOne(query, transaction)
	if err != nil {
		_, ok := err.(*bongo.DocumentNotFoundError)
		if !ok {
			return &model.Transaction{}, false, err
		}
		return &model.Transaction{}, false, nil
	}

	return transaction, true, nil
}

//PaymentSourceBalanceTransaction A payment source for a balance transaction
const PaymentSourceBalanceTransaction stripe.PaymentSourceType = "balance_transaction"