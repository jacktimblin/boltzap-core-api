package service

import (
	"errors"
	"sync"

	"github.com/nsqio/go-nsq"
	"core"
	"time"
)

const (
	//DefaultNsqLookupAddress The default nsq lookup, defaults to localhost.
	DefaultNsqLookupAddress = "localhost:4161"
	//DefaultConcurrencyLimit The default amount of consumers to spawn.
	DefaultConcurrencyLimit = 3
	//ConfigConsumer the key to access consumer settings in config.s
	ConfigConsumer = "consumer"
)

//ConsumerService A service to consuming messages from a NSQ cluster.
type ConsumerService struct {
	//The nsq lookupd address
	NsqLookupAddress string `json:"nsq_lookup_address"`

	//The handler func
	Handler nsq.HandlerFunc

	//The topic to subscribe to.
	Topic string `json:"topic"`

	//The channel.
	Channel string `json:"channel"`

	//The consumer instance.
	consumer *nsq.Consumer

	//The amount of consumers to spawn.
	ConcurrencyLimit int `json:"concurrency_limit"`

	//The amount of time between querying lookupd for new messages, default 60s
	LookupPollInterval core.Duration `json:"lookup_poll_interval"`
}

//NewConsumerService Creates a new consumer service.
func NewConsumerService() (*ConsumerService, error) {
	srv := &ConsumerService{
		NsqLookupAddress: DefaultNsqLookupAddress,
		Handler:          func(message *nsq.Message) error { return nil },
		ConcurrencyLimit: DefaultConcurrencyLimit,
		LookupPollInterval: core.Duration(time.Second * 60),
	}

	return srv, nil
}

//Consume Consumes events based on the Topic, Channel and Handler set in the ConsumerService
func (cs *ConsumerService) Consume() error {
	if cs.Handler == nil {
		return errors.New("handler must be defined")
	}
	if cs.NsqLookupAddress == "" {
		return errors.New("lookup address must be defined")
	}
	if cs.Topic == "" || cs.Channel == "" {
		return errors.New("topic and channel cannot be empty")
	}

	wg := sync.WaitGroup{}
	wg.Add(1)

	config := nsq.NewConfig()
	config.LookupdPollInterval = time.Duration(cs.LookupPollInterval)
	consumer, err := nsq.NewConsumer(cs.Topic, cs.Channel, config)
	if err != nil {
		return err
	}
	cs.consumer = consumer
	cs.consumer.AddConcurrentHandlers(nsq.Handler(cs.Handler), cs.ConcurrencyLimit)

	err = cs.consumer.ConnectToNSQLookupd(cs.NsqLookupAddress)

	if err != nil {
		return err
	}

	wg.Wait()

	return nil
}
