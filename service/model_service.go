package service

import (
	"github.com/maxwellhealth/bongo"
	"gopkg.in/mgo.v2"
)

//ModelService Used to perform tasks on models.
type ModelService struct {
	Database   DatabaseConnection
	Connection *bongo.Connection
	Config     *bongo.Config
}

//Model An instance of a model.
type Model interface {
	Indices(session *mgo.Session, config *bongo.Config) error
}

//Indices Runs model.Indices on all supplied models.
func (m *ModelService) Indices(models ...Model) error {
	for _, model := range models {
		err := model.Indices(m.Connection.Session, m.Config)
		if err != nil {
			return err
		}
	}

	return nil
}
