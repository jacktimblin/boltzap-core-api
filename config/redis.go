package config

import "core"

const (
	//ConfigRedis the config key used to get redis configuration.
	ConfigRedis = "redis"
)

//RedisConfig config to connect to redis.
type RedisConfig struct {
	Address core.URL `json:"address"`
	Password string `json:"password"`
	EncryptionKey string `json:"encryption_key"`
}
