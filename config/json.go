package config

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"errors"
)


//Config a config map after reading a .json file.
//Warning this is not goroutine safe so should not be shared between go-routines.
type Config map[string]*json.RawMessage

//ParseConfig parses config from a config json file supplied through flag.
func ParseConfig() (Config, error) {
	var configFile string
	flag.StringVar(&configFile, "config", "config.json", "JSON config file.")
	flag.Parse()

	if configFile == "" {
		flag.PrintDefaults()
		return Config{}, errors.New("config file not supplied")
	}

	data, err := ioutil.ReadFile(configFile)
	var objmap map[string]*json.RawMessage
	err = json.Unmarshal(data, &objmap)

	if err != nil {
		return Config{}, err
	}

	return Config(objmap), nil
}

//Has determines if the config map has a value.
func (c Config) Has(key string) bool {
	_, found := c[key]
	return found
}

//Get Gets a value from the config, mapping it to typ, dft is returned if
//the value does not exist.
func (c Config) Get(key string, dft interface{}) error {
	if !c.Has(key) {
		return nil
	}

	err := json.Unmarshal(*c[key], dft)

	if err != nil {
		return err
	}

	return nil
}
