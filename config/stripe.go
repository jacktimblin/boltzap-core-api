package config

import (
	"core"
)

const (
	//ConfigStripe the config key for stripe details.
	ConfigStripe = "stripe"
	ProtocolHttp = "http"
	ProtocolHttps = "https"
	DefaultPort = "8080"
	DefaultDomain = "localhost"
	DefaultPath = "/"
)

//Stripe stripe config.
type StripeConfig struct {
	//ApiKey the api used to access Stripe services.
	ApiKey string `json:"api_key"`
	//Client ID of out stripe application.
	ClientID string `json:"client_id"`
	//Handler the transaction-processor event handler url, used for processing rescheduling.
	Handler core.URL `json:"handler_endpoint"`
}
