package config

const (
	//ConfigXero config key for xero config.
	ConfigXero = "xero"
)

//XeroConfig xero api configuration.
type XeroConfig struct {
	ConsumerKey string `json:"consumer_key"`
	ConsumerSecret string `json:"consumer_secret"`
	PrivateKeyFile string `json:"private_key_file"`
	ApplicationType string `json:"application_type"`
	UserAgent string `json:"user_agent"`
	InternalBankAccount string `json:"internal_bank_account"`
}
