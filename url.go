package core

import (
	"net/url"
	"encoding/json"
)

//URL a custom implementation of url.URL which allows us to unmarshal it from JSON
type URL struct {
	url.URL
}

//UnmarshalJSON implements the interface which allows to unmarshal this type from json.
func (u *URL) UnmarshalJSON(data []byte) error {
	endpoint := new(string)
	if err := json.Unmarshal(data, endpoint); err != nil {
		return err
	}

	if *endpoint == "" {
		return nil
	}

	ul, err := url.ParseRequestURI(*endpoint)
	if err != nil {
		return err
	}

	u.URL = *ul

	return nil
}

//MarshalJSON implements a marshal which returns the url as a string.
func (u URL) MarshalJSON() ([]byte, error) {
	return json.Marshal(u.String())
}
