package model

import (
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/maxwellhealth/bongo"
	"gopkg.in/mgo.v2"
)

const (
	//UserCollection The collection for users in mongo db.
	UserCollection = "users"
	//RoleAdmin The admin role for a user.
	RoleAdmin = iota
	//RoleUser The standard user role.
	RoleUser
	//ScopeUserRead Scope level to read a users profile.
	ScopeUserRead
	//ScopeUserEdit Scope level to read and edit a users profile.
	ScopeUserEdit
	//ScopeAll System level scope which gives all permissions.
	ScopeAll
)

//Token an encapsulation of a token.
type Token struct {
	ClientID  string
	Token     string
	ExpiresAt int64
	Active    bool
	Scope     []int
}

//PasswordEntry an encapsulation of a password.
type PasswordEntry struct {
	Password string
	DateSet  time.Time
}

//UserClaims Custom JWTClaims object.
type UserClaims struct {
	Name string `json:"name"`
	Role int    `json:"-"`
	UUID string `json:"uuid"`
	jwt.StandardClaims
}

//HasScope determines if a token has access to a specific scope level.
func (t *Token) HasScope(scope int) bool {
	for _, sc := range t.Scope {
		if sc == scope || sc == ScopeAll {
			return true
		}
	}
	return false
}

//Stripe stripe credentials and settings.
type Stripe struct {
	AccountID    string
	AccessToken  string
	RefreshToken string
	Created      time.Time
	Updated      time.Time
	ProcessFees  bool
	ProcessCharges bool
	ProcessTransfers bool
	UpdateMetadata bool
	AutoReconcile bool
}

//StripeCustomer customer information we have stored on stripe.
//TODO - remove Customer* prefix from struct values.
type StripeCustomer struct {
	CustomerToken string
	CustomerCurrency string
	CustomerCardID string
	CustomerCard string
	CardExpiry string
	CardType string
	Modified time.Time
	Created time.Time
}

//User An encapsulation of a user.
type User struct {
	bongo.DocumentBase `bson:",inline"`
	//Any login tokens applied to this user.
	Tokens []Token
	//The users role.
	Role int
	//The username of the user, typically an email address.
	Username string
	//Whether the user has verified their email address.
	EmailVerified bool
	//A verification token used to uniquely identify this user when
	//verifying their email address.
	EmailToken Token
	//The users password.
	Password string
	//The profile of a user.
	Profile struct {
		//Users first name.
		FirstName string
		//Users last name.
		LastName string
		//Users job title/position.
		Position string
	}
	//The ID of the company the user belongs too.
	CompanyID string
	//A reset token which is used when a user attempts to reset their password.
	PasswordResetToken Token
	//A users password history.
	PasswordHistory []PasswordEntry
	//The users position in the `queue`
	QueuePosition int
	//The users connected stripe credentials.
	Stripe Stripe
	//The version of this document schema.
	Version float64
	//customer information we hold about this customer on stripe.
	StripeCustomer StripeCustomer
	//The unique id of the user.
	UUID string
	//Whether the user have initially added their card details.
	Subscribed bool
	//The current stripe subscription.
	StripeSubscription struct {
		SubscriptionID string
		EndDate time.Time
		Status string
		Name string
		PlanID string
		Amount int64
	}
}

//ConnectStripe Connects stripe credentials to a user.
func (u *User) ConnectStripe(stripe Stripe) {
	u.Stripe = stripe
}

//Indices implements Model, applies a index on queue position and on each token.
func (u *User) Indices(session *mgo.Session, config *bongo.Config) error {
	collection := session.DB(config.Database).C(UserCollection)

	indices := []mgo.Index{
		mgo.Index{
			Key:        []string{"queueposition"},
			Unique:     true,
			DropDups:   true,
			Background: true,
			Sparse:     true,
		},
		mgo.Index{
			Key:        []string{"tokens.token"},
			Unique:     true,
			DropDups:   true,
			Background: true,
			Sparse:     true,
		},
		mgo.Index{
			Key:        []string{"stripe.accountid"},
			Unique:     true,
			DropDups:   true,
			Background: true,
			Sparse:     true,
		},
		mgo.Index{
			Key:        []string{"uuid"},
			Unique:     true,
			DropDups:   true,
			Background: true,
			Sparse:     true,
		},
	}

	for _, index := range indices {
		err := collection.EnsureIndex(index)
		if err != nil {
			return err
		}
	}

	return nil
}

//GetExistingToken Checks to see if a user has an existing token.
func (u *User) GetExistingToken() (*Token, int, bool) {
	for i := range u.Tokens {
		token := &u.Tokens[i]
		expires := time.Unix(token.ExpiresAt, 0)
		if expires.After(time.Now()) && token.Active {
			return token, i, true
		}
	}

	return &Token{}, -1, false
}

//GetFullName Gets the users full name.
func (u *User) GetFullName() string {
	return u.Profile.FirstName + " " + u.Profile.LastName
}

//GetToken Gets a token pointer from a user for the provided signedString, active determines if the
//token also has to be active.
func (u *User) GetToken(tkn string, active ...bool) (*Token, int, bool) {
	a := false
	if len(active) > 0 {
		a = active[0]
	}
	for i := range u.Tokens {
		token := &u.Tokens[i]
		if token.Token == tkn {
			if a {
				if token.Active {
					return token, i, true
				}
			} else {
				return token, i, true
			}
		}
	}

	return &Token{}, -1, false
}

//RemoveToken removes the token at a particular index.
func (u *User) RemoveToken(i int) {
	u.Tokens = append(u.Tokens[:i], u.Tokens[i+1:]...)
}

//RemoveExpiredTokens removes all expires tokens from a user.
func (u *User) RemoveExpiredTokens() bool {
	ch := false
	for i := range u.Tokens {
		token := &u.Tokens[i]
		expires := time.Unix(token.ExpiresAt, 0)
		if expires.Before(time.Now()) || !token.Active {
			u.RemoveToken(i)
			ch = true
		}
	}
	if time.Unix(u.EmailToken.ExpiresAt, 0).Before(time.Now()) {
		u.EmailToken = Token{}
		ch = true
	}
	return ch
}

//AddToken Adds a token to this user.
func (u *User) AddToken(tkn Token) {
	u.Tokens = append(u.Tokens, tkn)
}

//HasValidEmailToken Determines if a user has a valid email token.
func (u *User) HasValidEmailToken() bool {
	return u.EmailToken.Active && time.Unix(u.EmailToken.ExpiresAt, 0).After(time.Now())
}

//HasValidPasswordResetToken Determines if a user has a valid password reset token.
func (u *User) HasValidPasswordResetToken() bool {
	return u.PasswordResetToken.Active && time.Unix(u.PasswordResetToken.ExpiresAt, 0).After(time.Now())
}

//SetPassword Sets this users password.
func (u *User) SetPassword(password []byte) {
	u.Password = string(password)
	entry := PasswordEntry{DateSet: time.Now(), Password: u.Password}
	u.PasswordHistory = append(u.PasswordHistory, entry)
}

//IsPasswordInHistory Checks if a password is in a users history,
//which was set after the provided from time.
func (u *User) IsPasswordInHistory(password []byte, months int) bool {
	for _, entry := range u.PasswordHistory {
		if string(password) == entry.Password {
			dt := entry.DateSet.AddDate(0, months, 0)
			if dt.After(time.Now()) {
				return true
			}
		}
	}

	return false
}
