package model

import (
	"github.com/maxwellhealth/bongo"
)

//Credentials Key and Secret for a client.
type Credentials struct {
	Key    string
	Secret string
}

//Client An encapsulation of a client.
type Client struct {
	bongo.DocumentBase `bson:",inline"`
	RedirectURL        string
	LiveCredentials    []Credentials
	TestCredentials    []Credentials
	ImageURL           string
}
