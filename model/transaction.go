package model

import (
	"time"
	"strings"
	"github.com/maxwellhealth/bongo"
	"gopkg.in/mgo.v2"
	"github.com/rhymond/go-money"
)

//TransactionCollection The transactions mongodb collection.
const TransactionCollection = "transactions"

//TransactionSchemaVersion the schema version of a transaction.
const TransactionSchemaVersion = 0.3

const (
	//GatewayStripe stripe.
	GatewayStripe int = iota
	//GatewayBraintree Braintree.
	GatewayBraintree
)

const (
	//TypeAccountDeauthorized the event when a user de-authorises our app.
	TypeAccountDeauthorized = "account.application.deauthorized"
	//TypeChargeCaptured the event when a stripe charge is captured.
	TypeChargeCaptured = "charge.captured"
	//TypeChargeRefunded the event when a stripe charge is refunded.
	TypeChargeRefunded = "charge.refunded"
	//TypeChargeSucceeded the event when a charge succeeds.
	TypeChargeSucceeded = "charge.succeeded"
	//TypePayoutPaid the event when a payout request is paid.
	TypePayoutPaid = "payout.paid"
	//TypeChargeFailed the event when a charge failed to complete.
	TypeChargeFailed = "charge.failed"
	//TypeSubcriptionCreated the event when a customer subscribes on a new plan.
	TypeSubcriptionCreated = "customer.subscription.created"
	//TypeSubscriptionDeleted the event when a user stops / cancels their subscription.
	TypeSubscriptionDeleted = "customer.subscription.deleted"
	//TypeSubscriptionTrialWillEnd the event when a customer is coming to the end of their free trial.
	TypeSubscriptionTrialWillEnd = "customer.subscription.trial_will_end"
	//TypeInvoiceCreated the event when stripe generates a new invoice.
	TypeInvoiceCreated = "invoice.created"
	//TypeInvoicePaymentFailed the event when a payment fails against an invoice.
	TypeInvoicePaymentFailed = "invoice.payment_failed"
	//TypeInvoicePaymentSucceeded the event when a payment succeeds against an invoice.
	TypeInvoicePaymentSucceeded = "invoice.payment_succeeded"
)

const (
	TypeCharge = "charge"
	TypeRefund = "refund"
	TypePayout = "payout"
	TypeFee = "fee"
)

//Customer details of a customer.
type Customer struct {
	//The gateways id for a customer.
	GatewayID string
	//The email of the customer, this is not required so this may be nil.
	Email string
}

//InvoiceDetails details of an invoice attached to a transaction.
type InvoiceDetails struct {
	InvoiceID string
	//TODO anything else we need to store to match an invoice to a transaction.
}

//Transaction An encapsulation of a transaction.
//TODO - this collection will need sharding.
type Transaction struct {
	bongo.DocumentBase `bson:",inline"`
	//The gateways ID of this transaction.
	GatewayID string
	//The gateways ID of the event which this charge was emitted on.
	GatewayEventID string
	//The time the transaction was created on the gateway.
	TransactionCreated time.Time
	//Whether the transaction has successfully been captured.
	Captured bool
	//Whether the transaction has been refunded at least once.
	Refunded bool
	//The amount which has been refunded. We could have a partial refund.
	RefundedAmount int64
	//The amount of the origin charge.
	Amount int64
	//The currency the payment is in.
	Currency string
	//The source of the transaction. This is only used internally for analytics.
	Source struct {
		//The type of source.
		Type string
		//The gateways id for this source.
		ID string
	}
	//The status of the transaction.
	Status string
	//The transactions description.
	Description string
	//The customer for the transaction
	Customer Customer
	//The gateway account id this transaction was for.
	GatewayAccountID string
	//The user id on our system this transaction was for.
	UserID string
	//The gateway this transaction was processed on.
	Gateway int
	//The type of transaction this represents.
	Type string
	//InvoiceDetails the details of the invoice this transaction represents.
	InvoiceDetails InvoiceDetails
	//The uuid of this transaction.
	UUID string
	//The amount of the charge converted to the users currency.
	ConvertedAmount int64
	//The currency the charge was converted into.
	ConvertedToCurrency string
	//The net total of the charge, after all fees are applied.
	NetAmount int64
	//The total amount of fees applied to this charge.
	TotalFees int64
	//The parent transaction uuid, if this transaction represents a fee
	//This will be the transaction the fee is for or if the type is a refund, we
	//can link what charge we are refunding.
	ParentTransaction struct {
		//The parent transaction uuid
		UUID string
		//The relationship to the parent. i.e a fee or a refund.
		Type string
	}
	//The schema version of this document.
	Version float64
	//ConnectTransaction Whether this transaction is a connect transaction
	//This is only applicable for Stripe transactions, as we use Stripe internally for stripe charges.
	ConnectTransaction bool
}

//Format Formats amount with the provided currency. amount is always in the currencies smallest currency unit.
func (t Transaction) Format(amount int64, currency string) string {
	return money.New(amount, currency).Display()
}

//FormatAmount Formats the amount into a string.
func (t Transaction) FormatAmount() string {
	return t.Format(t.Amount, strings.ToUpper(t.Currency))
}

//FormatConvertedAmount Formats the converted amount into a string.
func (t Transaction) FormatConvertedAmount() string {
	return t.Format(t.ConvertedAmount, strings.ToUpper(t.ConvertedToCurrency))
}

//Indices applies indices to the Transaction collection.
func (t *Transaction) Indices(session *mgo.Session, config *bongo.Config) error {
	collection := session.DB(config.Database).C(TransactionCollection)

	indices := []mgo.Index{
		mgo.Index{
			Key: []string{"gatewayid", "gateway"},
			Sparse: true,
			Unique: true,
			DropDups: true,
			Background: true,
		},
		mgo.Index{
			Key: []string{"userid"},
			Sparse: true,
			Background: true,
			DropDups: false,
			Unique: false,
		},
	}

	for _, index := range indices {
		err := collection.EnsureIndex(index)
		if err != nil {
			return err
		}
	}

	return nil
}
